<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*************************************************************
 * Program History 
 *
 * @Project		LUPPIE
 * @author		Desi Anggraeni
 * @Date		Maret 2016
 * @ProgramName	Core Controller LUPPIE
 * @ProgramId	-
 *
 ************************************************************/

class LUPPIE_Controller extends CI_Controller {
	
	public $data = array();

	function __construct(){
		parent::__construct();
           
			$this->load->model('User_model');
        	$this->load->model('Provinsi_model');
        	$this->load->model('Kokab_model');

        	$this->load->model('McDoctor_model');
        	$this->load->model('McApoteker_model');
        	$this->load->model('McKeluhan_model');
        	$this->load->model('McKomunikasi_model');
        	$this->load->model('McKonsultasiPelayananKefarmasian_model');
        	$this->load->model('McReaksiAlergi_model');
        	$this->load->model('McRencanaPemeriksaan_model');
        	$this->load->model('McRiwayatImunisasi_model');
        	$this->load->model('McRiwayatPengobatanDokter_model');
        	$this->load->model('McRiwayatPengobatanSendiri_model');
        	$this->load->model('McRiwayatRawatInap_model');
			
	}

	public function getProvinsi(){
		
		$q = $this->Provinsi_model->getAll();
		return $q;
	}

	public function getKokab(){
		
		$q = $this->Kokab_model->getAll();
		return $q;
	}

	public function getMcDoctorByIdPasien($id_pasien){
		return $this->McDoctor_model->getByIdPasien($id_pasien);	
	}
	public function getMcApotekerByIdPasien($id_pasien){
		return $this->McApoteker_model->getByIdPasien($id_pasien);	
	}
	public function getMcKeluhanByIdPasien($id_pasien){
		return $this->McKeluhan_model->getByIdPasien($id_pasien);	
	}
	public function getMcKomunikasiByIdPasien($id_pasien){
		return $this->McKomunikasi_model->getByIdPasien($id_pasien);	
	}
	public function getMcKonsultasiPelayananKefarmasianByIdPasien($id_pasien){
		return $this->McKonsultasiPelayananKefarmasian_model->getByIdPasien($id_pasien);	
	}
	public function getMcReaksiAlergiByIdPasien($id_pasien){
		return $this->McReaksiAlergi_model->getByIdPasien($id_pasien);	
	}
	public function getMcRencanaPemeriksaanByIdPasien($id_pasien){
		return $this->McRencanaPemeriksaan_model->getByIdPasien($id_pasien);	
	}
	public function getMcRiwayatImunisasiByIdPasien($id_pasien){
		return $this->McRiwayatImunisasi_model->getByIdPasien($id_pasien);	
	}
	public function getMcRiwayatPengobatanDokterByIdPasien($id_pasien){
		return $this->McRiwayatPengobatanDokter_model->getByIdPasien($id_pasien);	
	}
	public function getMcRiwayatPengobatanSendiriByIdPasien($id_pasien){
		return $this->McRiwayatPengobatanSendiri_model->getByIdPasien($id_pasien);	
	}
	public function getMcRiwayatRawatInapByIdPasien($id_pasien){
		return $this->McRiwayatRawatInap_model->getByIdPasien($id_pasien);	
	}

	/*  Function validateEmail
		DAS - 12032016
		RETURN
		TRUE - If Valid
		FALSE - If Not Valid
	*/
    public function validateEmail($email){
    	if(filter_var($email, FILTER_VALIDATE_EMAIL))
    		return true;
    	return false;
	}

	/*  Function isEmailRegistered
		DAS - 12032016
		RETURN
		TRUE - If Registered
		FALSE - If Not Yet Register
	*/
    public function isEmailRegistered($email){
    	if($this->User_model->validateUser($email))
    		return true;
    	return false;
	}

	/*  Function getIdUserByEmail
		DAS - 12032016
		RETURN Id User
	*/
    public function getIdUserByEmail($email){
    	return $this->User_model->getIdUserByEmail($email);
	}

	/*  Function getMessage
		DAS - 12032016
		RETURN Desc Msg
	*/
    public function getMessage($idMsg){
    	$msg = "";

    	switch($idMsg){
    		case 0: $msg = "OPERATION FAILED"; break;
    		case 1: $msg = "OPERATION SUCCESSFULL"; break;
    		case 2: $msg = "INVALID EMAIL ADDRESS "; break;
    		case 3: $msg = "EMAIL REDUNDANT"; break;
    		case 4: $msg = "EMAIL NOT YET REGISTERED"; break;
    		default: $msg = "No Message"; break;
    	}

    	return $msg;
	}
	
    public function haveAccess(){
        $res = $this->m_privilege->getAccess($this->c_menu_id,  $this->user['id_user_group']);
        return $res;
	}
    
    public function notFound(){
        $data['title'] = "404 Not Found";
        $data['nav'] = "Not Found";
		$data['page'] = $this->load->view("errors/html/error_404", '', true);
        $this->display($data);   
        
    }
    
}