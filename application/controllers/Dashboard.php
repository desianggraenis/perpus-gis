<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');  
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

/*
 * User Controller
 * DAS - 12032016
 */

class Dashboard extends CI_Controller {  //lUPPIE_Controller {

	public function Dashboard() {
        parent::__construct();
        
        $this->load->model('m_perpus');
    }

	public function index()
	{
		$list_perpus = $this->m_perpus->getAll();
		$data['menu'] = "clustering";
		$data['submenu'] = "clustering";
		$data['list_perpus'] =  json_encode($list_perpus, JSON_NUMERIC_CHECK);
        $data['page_clustering'] = $this->load->view("clustering", $data, true);
		$this->load->view('index', $data); //index3
	}

	public function perpusType()
	{
		$data['perpusType'] = $this->m_perpus->getPerpusType();
		// $this->load->view('combining-ind2', $data);
		$data['menu'] = "graduated";
		$data['submenu'] = "perpustype";
        $data['page'] = $this->load->view("perpustype", $data, true);
		$this->load->view('index', $data);
	}

	public function pustakawan()
	{
		$data['pustakawan'] = $this->m_perpus->getPustakawan();
		// $this->load->view('comb_pustakawan', $data);
		$data['menu'] = "graduated";
		$data['submenu'] = "pustakawan";
        $data['page'] = $this->load->view("pustakawan", $data, true);
		$this->load->view('index', $data);
	}

	public function koleksi()
	{
		$data['koleksi'] = $this->m_perpus->getKoleksi();
		// $this->load->view('comb_koleksi', $data);
		$data['menu'] = "graduated";
		$data['submenu'] = "koleksi";
        $data['page'] = $this->load->view("koleksi", $data, true);
		$this->load->view('index', $data);
	}

	public function pengunjung()
	{
		$data['pengunjung'] = $this->m_perpus->getPengunjung();
		// $this->load->view('comb_pengunjung', $data);
		$data['menu'] = "graduated";
		$data['submenu'] = "pengunjung";
        $data['page'] = $this->load->view("pengunjung", $data, true);
		$this->load->view('index', $data);
	}

	public function chartPerpusType()
	{
		$list = $this->m_perpus->getChartLin();
		$data['list'] = $list;
		$data['listjson'] =  json_encode($list, JSON_NUMERIC_CHECK);

		$dataPerpus = array();
		foreach ($list as $key) {

			//print_r($key->name);
			
			$row = array(
				"title" => $key->name,
				"latitude" => $key->lat,
				"longitude" => $key->lang,
				"width" => 100,
				"height" => 100,
				"pie" => array(
					"type" => "pie",
					"pullOutRadius" => 0,
					"labelRadius" => 0,
					"dataProvider" => array(
						array(
						  "category" => "Perpustakaan Umum Provinsi",
						  "value" => $key->a
						),
						array(
						  "category" => "Perpustakaan Umum Kabupaten/Kota",
						  "value" => $key->b
						),
						array(
						  "category" => "Perpustakaan Kecamatan",
						  "value" => $key->c
						),
						array(
						  "category" => "Perpustakaan Khusus",
						  "value" => $key->d
						),
						array(
						  "category" => "Perpustakaan Sekolah",
						  "value" => $key->e
						),
						array(
						  "category" => "Perpustakaan Perguruan Tinggi",
						  "value" => $key->f
						),
						array(
						  "category" => "Perpustakaan Khusus - Instansi Pemerintah",
						  "value" => $key->g

						)
					),
					// "labelText": "[[value]]%",
					"labelText" => "",
					"valueField" => "value",
					"titleField" => "category"
				)
				
			);

      		$dataPerpus[] = $row;
      
		}

		$data['dataPerpus'] = json_encode($dataPerpus);

		// $this->load->view('chart2', $data);
		$data['menu'] = "chart";
		$data['submenu'] = "perpustype";
        $data['page'] = $this->load->view("chart_temp", $data, true);
		$this->load->view('index', $data);
	}


	public function chartPengunjung()
	{
		$list = $this->m_perpus->getChartPengunjung();
		$data['list'] = $list;
		$data['listjson'] =  json_encode($list, JSON_NUMERIC_CHECK);

		$dataPerpus = array();
		foreach ($list as $key) {

			//print_r($key->name);
			
			$row = array(
				"title" => $key->name,
				"latitude" => $key->lat,
				"longitude" => $key->lang,
				"width" => 100,
				"height" => 100,
				"pie" => array(
					"type" => "pie",
					"pullOutRadius" => 0,
					"labelRadius" => 0,
					"dataProvider" => array(
						array(
						  "category" => "Pelajar",
						  "value" => $key->a
						),
						array(
						  "category" => "Mahasiswa",
						  "value" => $key->b
						),
						array(
						  "category" => "Guru",
						  "value" => $key->c
						),
						array(
						  "category" => "Dosen",
						  "value" => $key->d
						),
						array(
						  "category" => "PNS",
						  "value" => $key->e
						),
						array(
						  "category" => "Karyawan",
						  "value" => $key->f
						),
						array(
						  "category" => "Wiraswasta",
						  "value" => $key->g

						),
						array(
						  "category" => "TNI",
						  "value" => $key->h

						),
						array(
						  "category" => "Umum",
						  "value" => $key->i

						)
					),
					// "labelText": "[[value]]%",
					"labelText" => "",
					"valueField" => "value",
					"titleField" => "category"	
				)
				
			);

      		$dataPerpus[] = $row;
      
		}

		$data['dataPerpus'] = json_encode($dataPerpus);
		// $this->load->view('chart2', $data);

		$data['menu'] = "chart";
		$data['submenu'] = "pengunjung";
        $data['page'] = $this->load->view("chart_temp", $data, true);
		$this->load->view('index', $data);
	}

	public function getByType($type)
	{
		$list = $this->m_perpus->getByPerpusType($type);
		//$data =  json_encode($list, JSON_NUMERIC_CHECK);

		$dataPerpus = array(array("1","state"));
		foreach ($list as $key) {

			//print_r($key->name);
			
			$row = array($key->cnt,$key->prov);

      		$dataPerpus[] = $row;
      
		}

		// print_r($dataPerpus);
		// die();
		$data['dataPerpus'] = json_encode($dataPerpus);

		echo $data['dataPerpus'];
	}

	public function getByPustakawan($type)
	{
		$list = $this->m_perpus->getByPustakawan($type);
		//$data =  json_encode($list, JSON_NUMERIC_CHECK);

		$dataPerpus = array(array("1","state"));
		foreach ($list as $key) {

			//print_r($key->name);
			
			$row = array($key->cnt,$key->prov);

      		$dataPerpus[] = $row;
      
		}

		// print_r($dataPerpus);
		// die();
		$data['dataPerpus'] = json_encode($dataPerpus);

		echo $data['dataPerpus'];
	}

	public function getByKoleksi($type)
	{
		$list = $this->m_perpus->getByKoleksi($type);
		//$data =  json_encode($list, JSON_NUMERIC_CHECK);

		$dataPerpus = array(array("1","state"));
		foreach ($list as $key) {

			//print_r($key->name);
			
			$row = array($key->cnt,$key->prov);

      		$dataPerpus[] = $row;
      
		}

		// print_r($dataPerpus);
		// die();
		$data['dataPerpus'] = json_encode($dataPerpus);

		echo $data['dataPerpus'];
	}

	public function getByPengunjung($type)
	{
		$list = $this->m_perpus->getByPengunjung($type);
		//$data =  json_encode($list, JSON_NUMERIC_CHECK);

		$dataPerpus = array(array("1","state"));
		foreach ($list as $key) {

			//print_r($key->name);
			
			$row = array($key->cnt,$key->prov);

      		$dataPerpus[] = $row;
      
		}

		// print_r($dataPerpus);
		// die();
		$data['dataPerpus'] = json_encode($dataPerpus);

		echo $data['dataPerpus'];
	}

	public function clustering()
	{
		$list_perpus = $this->m_perpus->getAll();
		$data['list_perpus'] =  json_encode($list_perpus, JSON_NUMERIC_CHECK);
		$this->load->view('index3', $data);
	}

	public function indonesia()
	{
		//$list_perpus = $this->m_perpus->getAll();
		//$data['list_perpus'] =  json_encode($list_perpus, JSON_NUMERIC_CHECK);

		// $lib_type = $this->m_perpus->getLibType();
		// $data['lib_type'] =  json_encode($lib_type);

		$this->load->view('combining-ind');
	}


	public function samplecombining()
	{
		//$list_perpus = $this->m_perpus->getAll();
		//$data['list_perpus'] =  json_encode($list_perpus, JSON_NUMERIC_CHECK);

		// $lib_type = $this->m_perpus->getLibType();
		// $data['lib_type'] =  json_encode($lib_type);

		$this->load->view('combining');
	}

	public function combining()
	{
		//$list_perpus = $this->m_perpus->getAll();
		//$data['list_perpus'] =  json_encode($list_perpus, JSON_NUMERIC_CHECK);

		// $lib_type = $this->m_perpus->getLibType();
		// $data['lib_type'] =  json_encode($lib_type);

		$this->load->view('index-comb');
	}

	public function chart()
	{
		//$list_perpus = $this->m_perpus->getAll();
		//$data['list_perpus'] =  json_encode($list_perpus, JSON_NUMERIC_CHECK);

		// $lib_type = $this->m_perpus->getLibType();
		// $data['lib_type'] =  json_encode($lib_type);

		$this->load->view('chart');
	}
	
}
