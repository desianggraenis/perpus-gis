
    <div id="map"></div>
    <script>

      function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: {lat: -2.114086, lng: 108.283203125}
        });

        var contentString = '<div id="content-frame"><iframe src="http://pemetaan.perpusnas.go.id/library/profile/1" frameborder="0" width="200px" height="100%"></iframe></div>';
        var infowindow = new google.maps.InfoWindow({
          content: contentString,
          maxWidth: 10
        });

        // Create an array of alphabetical characters used to label the markers.
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        var iconBase = 'http://pemetaan.perpusnas.go.id/';
        
        // var locations = <?=$list_perpus?> 

        // Create markers.
        var markers = locations.map(function(location, i) {
          // return new google.maps.Marker({
          //   position: new google.maps.LatLng(location.lat, location.lng),
          //   icon: iconBase + location.icon,
          //   draggable: true,
          //   animation: google.maps.Animation.DROP
          //   // label: labels[i % labels.length]
          // });

          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(location.lat, location.lng),
            icon: iconBase + location.icon,
            draggable: true,
            animation: google.maps.Animation.DROP
          });
          google.maps.event.addListener(marker, 'click', function(evt) {
            // infoWin.setContent(location.info);
            infowindow.open(map, marker);
          })

          // marker.addListener('click', toggleBounce);
          return marker;
        });


        // markers.addListener('click', function() {
        //   infowindow.open(map, marker);
        // });
        // Add some markers to the map.
        // Note: The code uses the JavaScript Array.prototype.map() method to
        // create an array of markers based on a given "locations" array.
        // The map() method here has nothing to do with the Google Maps API.
        /*
        var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            draggable: true,
            animation: google.maps.Animation.DROP,
            label: labels[i % labels.length]
          });
        });
        */

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});


        // $("#map").attr("style", " position: initial !important; ");
        // markers.addListener('click', toggleBounce);
      }


      // function toggleBounce() {
      //   if (marker.getAnimation() !== null) {
      //     marker.setAnimation(null);
      //   } else {
      //     marker.setAnimation(google.maps.Animation.BOUNCE);
      //   }
      // }

      var locations = <?=$list_perpus?> 
     

    </script>
  
