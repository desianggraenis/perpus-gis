<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/ammap_amcharts_extension.js"></script>
<script src="https://www.amcharts.com/lib/3/maps/js/continentsLow.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<style type="text/css">
	.amcharts-main-div {
    font-size: 4px;
  }
  #chartdiv {
    width: 100%;
    height: 600px;
  }
.amcharts-chart-div a {display:none !important;}
</style>

<div id="chartdiv"></div>
<!-- <div id="legenddiv" style="border: 2px dotted #3f3; margin: 5px 0 20px 0;"></div> -->
    <script>
/**
 * Create a map
 */
var map = AmCharts.makeChart("chartdiv", {
  "type": "map",
  "theme": "light",
  "projection": "winkel3",
  // "labelsEnabled": false,
  // "autoMargins": false,
  // "marginTop": 0,
  // "marginBottom": 0,
  // "marginLeft": 0,
  // "marginRight": 0,
  // "pullOutRadius": 0,
  // "balloon": {
  //   "adjustBorderColor": true,
  //   "color": "#000000",
  //   "cornerRadius": 5,
  //   "fillColor": "#FFFFFF"
  // },
  // "legend": {
  //     "align": "center",
  //     "position": "bottom",
  //     "marginRight": 21,
  //     "markerType": "circle",
  //     "right": -4,
  //     "labelText": "[[title]]",
  //     "valueText": " $[[value]] [[percents]]%",
  //     "valueWidth": 100,
  //     "textClickEnabled": true
  // },
  /**
   * Data Provider
   * The images contains pie chart information
   * The handler for `positionChanged` event will take care
   * of creating external elements, position them and create
   * Pie chart instances in them
   */
  "dataProvider": {
    "map": "continentsLow",
    "images": <?=$dataPerpus?>
  },
  
  /**
   * Add event to execute when the map is zoomed/moved
   * It also is executed when the map first loads
   */
  "listeners": [{
    "event": "positionChanged",
    "method": updateCustomMarkers
  }]
  
});

map.addListener("rendered", function () {
    map.initialZoomLatitude = -2.114086;
    map.initialZoomLongitude = 117.283203125;
    
		zoomIn();
});

function zoomIn () {
    centerMap();
    map.zoomIn();
}

function zoomOut () {
    centerMap();
    map.zoomOut();
}

function centerMap () {
    map.zoomToLongLat(4, map.initialZoomLongitude, map.initialZoomLatitude, true);
    
}

/**
 * Creates and positions custom markers (pie charts)
 */
function updateCustomMarkers(event) {
  // get map object
  var map = event.chart;

  // go through all of the images
  for (var x = 0; x < map.dataProvider.images.length; x++) {

    // get MapImage object
    var image = map.dataProvider.images[x];

    // Is it a Pie?
    if (image.pie === undefined) {
      continue;
    }

    // create id
    if (image.id === undefined) {
      image.id = "amcharts_pie_" + x;
    }
    // Add theme
    if ("undefined" == typeof image.pie.theme) {
      image.pie.theme = map.theme;
    }

    // check if it has corresponding HTML element
    if ("undefined" == typeof image.externalElement) {
      image.externalElement = createCustomMarker(image);
    }

    // reposition the element accoridng to coordinates
    var xy = map.coordinatesToStageXY(image.longitude, image.latitude);
    image.externalElement.style.top = xy.y + "px";
    image.externalElement.style.left = xy.x + "px";
    image.externalElement.style.marginTop = Math.round(image.height / -2) + "px";
    image.externalElement.style.marginLeft = Math.round(image.width / -2) + "px";
  }
}

/**
 * Creates a custom map marker - a div for container and a
 * pie chart in it
 */
function createCustomMarker(image) {

  // Create chart container
  var holder = document.createElement("div");
  holder.id = image.id;
  holder.title = image.title;
  holder.style.position = "absolute";
  holder.style.width = image.width-50 + "px";
  holder.style.height = image.height-50 + "px";

  // Append the chart container to the map container
  image.chart.chartDiv.appendChild(holder);

  // Create a pie chart
  var chart = AmCharts.makeChart(image.id, image.pie);
  // chart.addLegend(legend, "legenddiv");
  return holder;
}
    </script>