<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>SIG PERPUS</title>


    <link rel="stylesheet" href="<?=base_url()?>assets/admin/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/bootstrap.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/neon-core.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/neon-theme.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/neon-forms.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/custom.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/admin/css/skins/blue.css">

    <!--script src="<?=base_url()?>assets/admin/js/jquery-1.11.0.min.js"></script-->
    <script src="<?=base_url()?>assets/admin/jquery-2.2.3.min.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
        position:none;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }

      #content-frame {
        width: 1000px !important;
        height: 750px !important;
      }

      .gm-style-iw {
          height: 750px !important;
          width: 1000px !important;
      }

       .nicebox {
          position: absolute;
          text-align: center;
          font-family: "Roboto", "Arial", sans-serif;
          font-size: 13px;
          z-index: 5;
          box-shadow: 0 4px 6px -4px #333;
          padding: 5px 10px;
          background: rgb(255,255,255);
          background: linear-gradient(to bottom,rgba(255,255,255,1) 0%,rgba(245,245,245,1) 100%);
          border: rgb(229, 229, 229) 1px solid;
        }
        #controls {
          top: 10px;
          left: 110px;
          /*width: 360px;
          height: 45px;*/
        }
        #data-box {
          top: 10px;
          left: 500px;
          height: 45px;
          line-height: 45px;
          display: none;
        }
        #census-variable {
          width: 360px;
          height: 20px;
        }
        #legend { display: flex; display: -webkit-box; padding-top: 7px }
        .color-key {
          background: linear-gradient(to right,
            hsl(5, 69%, 54%) 0%,
            hsl(29, 71%, 51%) 17%,
            hsl(54, 74%, 47%) 33%,
            hsl(78, 76%, 44%) 50%,
            hsl(102, 78%, 41%) 67%,
            hsl(127, 81%, 37%) 83%,
            hsl(151, 83%, 34%) 100%);
          flex: 1;
          -webkit-box-flex: 1;
          margin: 0 5px;
          text-align: left;
          font-size: 1.0em;
          line-height: 1.0em;
        }
        #data-value { font-size: 2.0em; font-weight: bold }
        #data-label { font-size: 2.0em; font-weight: normal; padding-right: 10px; }
        #data-label:after { content: ':' }
        #data-caret { margin-left: -5px; display: none; font-size: 14px; width: 14px}

    </style>
  </head>
  <body class="page-body skin-blue" data-url="http://neon.dev">


<div class="page-container"> <!--sidebar-collapsed"> <!-- to close sidebar by default, "chat-visible" to make chat appear always --> 
  
  <div class="sidebar-menu">
    
      
    <header class="logo-env">
      
      <!-- logo -->
      <div class="logo">
        <a href="index.html"><!-- SISTEM INFORMASI GEOGRAFIS PERPUSTAKAAN -->
                <img src="<?=base_url()?>assets/admin/img/logo.png" width="240" alt="" class="white">
        </a>
      </div>
      
                  
      
      <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
      <div class="sidebar-mobile-menu visible-xs">
        <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
          <i class="entypo-menu"></i>
        </a>
      </div>
      
    </header>
        
    
    
    
        
    <ul id="main-menu" class="">
      <!-- add class "multiple-expanded" to allow multiple submenus to open -->
      <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
      <!-- Search Bar -->
      <li <?=($menu=="clustering")?' class="opened active"':''?>>
        <a href="<?=base_url()?>dashboard">
          <i class="entypo-gauge"></i>
          <span>Clustering Perpustakaan Nasional</span>
        </a>
      </li>
      <li <?=($menu=="graduated")?' class="opened active"':''?> >
        <a href="layout-api.html">
          <i class="entypo-layout"></i>
          <span>Graduated Analysis</span>
        </a>
        <ul>
          <li <?=($submenu=="perpustype")?' class="opened active"':''?>>
            <a href="<?=base_url()?>dashboard/perpusType">
              <span>Berdasarkan Jenis Perpustakaan</span>
            </a>
          </li>
          <li <?=($submenu=="pustakawan")?' class="opened active"':''?>>
            <a href="<?=base_url()?>dashboard/pustakawan">
              <span>Berdasarkan Jumlah Pustakawan</span>
            </a>
          </li>
          <li <?=($submenu=="pengunjung")?' class="opened active"':''?>>
            <a href="<?=base_url()?>dashboard/pengunjung">
              <span>Berdasarkan Jumlah Pengunjung</span>
            </a>
          </li>
          <li <?=($submenu=="koleksi")?' class="opened active"':''?>>
            <a href="<?=base_url()?>dashboard/koleksi">
              <span>Berdasarkan Jenis Koleksi</span>
            </a>
          </li>
        </ul>
      </li>
      <li <?=($menu=="chart")?' class="opened active"':''?>>
        <a href="layout-api.html">
          <i class="entypo-layout"></i>
          <span>Chart Analysis</span>
        </a>
        <ul>
          <li  <?=($submenu=="perpustype")?' class="opened active"':''?>>
            <a href="<?=base_url()?>dashboard/chartPerpusType">
              <span>Berdasarkan Jenis Perpustakaan</span>
            </a>
          </li>
          <li  <?=($submenu=="pengunjung")?' class="opened active"':''?>>
            <a href="<?=base_url()?>dashboard/chartPengunjung">
              <span>Berdasarkan Jumlah Pengunjung</span>
            </a>
          </li>
        </ul>
      </li>
    </ul>
    
    <footer class="main text-white">
      
     <strong class="center">DAS &copy; 2017 </strong>  
      
    </footer> 
  </div>  

   <?php if(isset($page)){ ?>  
  <div class="main-content">
  <?=$page?>
    
  </div>
  <?php } ?>
</div>
  <?=(isset($page_clustering))?$page_clustering:""?>  


  <!-- Bottom Scripts -->
  
  <script src="<?=base_url()?>assets/admin/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
  <script src="<?=base_url()?>assets/admin/js/bootstrap.js"></script>
  <script src="<?=base_url()?>assets/admin/js/joinable.js"></script>
  <script src="<?=base_url()?>assets/admin/js/resizeable.js"></script>
  <script src="<?=base_url()?>assets/admin/js/neon-custom.js"></script>
  <script src="<?=base_url()?>assets/admin/js/gsap/main-gsap.js"></script>
  <script src="<?=base_url()?>assets/admin/jquery.dataTables.min.js"></script>
  <script src="<?=base_url()?>assets/admin/js/dataTables.bootstrap.js"></script>

   
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCw_7hHBjATU71IbZ73fnQtgY_8YnLkopY&callback=initMap">
    </script>
  </body>
</html>