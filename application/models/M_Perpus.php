 <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_perpus extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->table = 'tb_perpus';
    }

    function getAll(){
        $this->db->select('latitude lat, longitude lng, libtype type, marker_icon icon');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    // function getLibType(){
    //     $this->db->select('DISTINCT libtype type, marker_icon icon');
    //     $query = $this->db->get($this->table);
    //     return $query->result();
    // }

    function getChart(){
         $query = $this->db->query("SELECT id prov, tb_prov.name, lat, lang, libtype_id type, libtype_name typename, COUNT( libid ) cnt
                            FROM tb_prov 
                            INNER JOIN tb_libtype
                            LEFT JOIN tb_perpus ON prov_id = id
                            AND libtype = libtype_id
                            GROUP BY id, tb_prov.name, lat, lang, libtype_id, typename");
        return $query->result();

    }
   
    function getChartLin(){
        $query = $this->db->query("SELECT id prov, tb_prov.name, lat, lang, 
        (select count(*) from tb_perpus where prov_id = id and libtype = 1) a,
        (select count(*) from tb_perpus where prov_id = id and libtype = 2) b,
        (select count(*) from tb_perpus where prov_id = id and libtype = 3) c,
        (select count(*) from tb_perpus where prov_id = id and libtype = 4) d,
        (select count(*) from tb_perpus where prov_id = id and libtype = 5) e,
        (select count(*) from tb_perpus where prov_id = id and libtype = 6) f,
        (select count(*) from tb_perpus where prov_id = id and libtype = 7) g
        FROM tb_prov ");
        return $query->result();

    }


    function getChartPengunjung(){
        $query = $this->db->query("SELECT id prov, tb_prov.name, lat, lang, 
        (select sum(jumlah) from tb_perpus_pengunjung, tb_perpus where prov_id = id and perpus_id = libid and pengunjung_id = 1) a,
        (select sum(jumlah) from tb_perpus_pengunjung, tb_perpus where prov_id = id and perpus_id = libid and pengunjung_id = 2) b,
        (select sum(jumlah) from tb_perpus_pengunjung, tb_perpus where prov_id = id and perpus_id = libid and pengunjung_id = 3) c,
        (select sum(jumlah) from tb_perpus_pengunjung, tb_perpus where prov_id = id and perpus_id = libid and pengunjung_id = 4) d,
        (select sum(jumlah) from tb_perpus_pengunjung, tb_perpus where prov_id = id and perpus_id = libid and pengunjung_id = 5) e,
        (select sum(jumlah) from tb_perpus_pengunjung, tb_perpus where prov_id = id and perpus_id = libid and pengunjung_id = 6) f,
        (select sum(jumlah) from tb_perpus_pengunjung, tb_perpus where prov_id = id and perpus_id = libid and pengunjung_id = 7) g,
        (select sum(jumlah) from tb_perpus_pengunjung, tb_perpus where prov_id = id and perpus_id = libid and pengunjung_id = 8) h,
        (select sum(jumlah) from tb_perpus_pengunjung, tb_perpus where prov_id = id and perpus_id = libid and pengunjung_id = 9) i
                                    
        FROM tb_prov ");
        return $query->result();

    }

    function getPerpusType(){
        $query = $this->db->query("SELECT libtype_id, libtype_name FROM tb_libtype ");
        return $query->result();
    }

    function getByPerpusType($type){
        $query = $this->db->query("SELECT (select count(*) from tb_perpus where prov_id = id and libtype = $type) cnt,id prov
                                    FROM tb_prov 
        ");
        return $query->result();
    }



    function getPustakawan(){
        $query = $this->db->query("SELECT id, pustaka_name FROM tb_pustakawan ");
        return $query->result();
    }

    function getByPustakawan($type){
        $query = $this->db->query("SELECT (select count(*) from tb_prov_pustakawan where prov_id = id and pustaka_id = $type) cnt,id prov
                                    FROM tb_prov 
        ");
        return $query->result();
    }


    function getKoleksi(){
        $query = $this->db->query("SELECT id, koleksi_name FROM tb_koleksi ");
        return $query->result();
    }

    function getByKoleksi($type){
        $query = $this->db->query("SELECT (select jumlah from tb_prov_koleksi where prov_id = id and koleksi_id = $type) cnt,id prov
                                    FROM tb_prov 
        ");
        return $query->result();
    }

    function getPengunjung(){
        $query = $this->db->query("SELECT id, pengunjung_nama FROM tb_pengunjung ");
        return $query->result();
    }

    function getByPengunjung($type){
        $query = $this->db->query("SELECT (select sum(jumlah) from tb_perpus_pengunjung, tb_perpus
                                where prov_id = id
                                  and perpus_id = libid 
                                  and pengunjung_id = $type) cnt,id prov
                                    FROM tb_prov 
        ");
        return $query->result();
    }
}

?>