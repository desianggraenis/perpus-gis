###################
SISTEM INFORMSI GEOGRAFIS PERPUSNAS
###################

Aplikasi Web GIS Analisa Data Perpustakaan Nasional Indonesia. Aplikasi Sistem Informasi Geografis ini merekapitulasi dan menganalisa data yang berkaitan dengan perpustakaan. Data Perpustakaan berdasarkan jenis perpustakaan, jumlah pengunjung, pustakawan, dan koleksi buku disajikan dalam bentuk grafis analisa chart dan graduated analysis (sebaran data berbentuk warna dengan basis frekwensi data tertentu). 

***************
Acknowledgement
***************

Sistem ini dibuat untuk memenuhi syarat tugas mata kuliah GIS.
DAS 2017.